/**
 * Nahdi Default Theme
 * @author    Yannick Tonta <yannick.tonta@redboxdigital.com>
 * @copyright Copyright (c) 2018 Redbox Digital (http://www.redboxdigital.com)
 */
define([
    'jquery',
    'jquery/slick',
    'jquery/match-height',
    'domReady!'
], function($) {
    return function() {

        var rbd = rbd || {}; // if namespace is not defined, make it equal to an empty object

        rbd.cw = {
            init: function() {
                this._initCarousel();
            },
            _initCarousel: function() {
                var widgetProductSelection = '.widget-product-selection';
                var productItemName = '.widget-product-selection .product-item-name';

                $(widgetProductSelection).not('.slick-initialized').slick();
                
                window.addEventListener('resize', $(productItemName).matchHeight(), false);
                window.addEventListener('orientationchange', $(productItemName).matchHeight());
            }
        }

        rbd.cw.init();
    }
});
