/**
 * Copyright © 2018 Redbox Digital Limited. All rights reserved.
 */

var config = {
    "map": {
        "*": {
            "carouselWidget": "Redbox_ProductSelectionWidget/js/carousel-widget" 
        }
    }
};
