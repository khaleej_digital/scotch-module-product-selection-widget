<?php 
/**
 * @category  RedboxDigital
 * @package   Redbox_ProductSelectionWidget
 * @copyright Copyright (c) 2016 Redbox Digital (http://www.redboxdigital.com)
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Redbox_ProductSelectionWidget',
    __DIR__
);