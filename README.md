Redbox Digital - Product Selection Widget - Magento 2
======================
This widget is a copy of the one inside  Redbox_CategoryLandingPages <https://bitbucket.org/redboxdigital/redbox-module-category-landing-pages>      

This widget can be used to select product to display in a carousel or simple grid template


######Author   
######Bolaji Olubajo <bolaji.tolulope@redboxdigital.com>  
######Elodie Gau <elodie.gau@redboxdigital.com>  