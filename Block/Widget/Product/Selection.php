<?php
/**
 * Product selection block/widget
 *
 * @category  RedboxDigital
 * @package   Redbox_ProductSelectionWidget
 * @author    Steve Desmet <steve.desmet@redboxdigital.com>
 *            Bolaji Olubajo <bolaji.tolulope@redboxdigital.com>
 * @copyright Copyright (c) 2016 Redbox Digital (http://www.redboxdigital.com)
 */
namespace Redbox\ProductSelectionWidget\Block\Widget\Product;

use Magento\Framework\Registry;
use Magento\Widget\Block\BlockInterface;
use Magento\Customer\Model\Context as CustomerContext;
use Magento\Catalog\Block\Product\Context as ProductContext;

class Selection extends \Magento\Catalog\Block\Product\NewProduct
    implements BlockInterface
{

    /** @var Registry */
    private $registry;

    /**
     * @param ProductContext $context
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param array $data
     */
    public function __construct(
        ProductContext $context, \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $catalogProductVisibility,
        \Magento\Framework\App\Http\Context $httpContext, array $data
    ) {
        parent::__construct($context, $productCollectionFactory, $catalogProductVisibility, $httpContext, $data);
    }

    /**
     * Get products collection
     *
     * @author Steve Desmet <steve.desmet@redboxdigital.com>
     *         Bolaji Olubajo <bolaji.tolulope@redboxdigital.com>
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected function _getProductCollection()
    {
        $productIds = explode(',', $this->getProducts());
        if (!$productIds) {
            return array();
        }
        /** @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->_productCollectionFactory->create()
            ->addStoreFilter()
            ->addIdFilter($productIds)
            ->setPageSize($this->getProductsCount())
            ->setCurPage(1)
            ->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());
        $this->_addProductAttributesAndPrices($collection);
        return $collection;
    }

    /**
     * Helper function to preserve product Ids order
     *
     * @param array $items
     * @return array
     */
    public function preserveProductIdsOrder(array $items)
    {
        $productIds = explode(',', $this->getProducts());
        $sorted = [];
        if ($productIds) {
            foreach($productIds as $id) {
                if (isset($items[$id])) {
                    $sorted[] = $items[$id];
                }
            }
        }
        return !empty($sorted) ? $sorted : $items;
    }

    /**
     * Prepare collection with selected products
     *
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $this->setProductCollection($this->_getProductCollection());
        return parent::_beforeToHtml();
    }

    /**
     * Get Key pieces for caching block content
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return [
            'CATALOG_PRODUCT_SELECTION',
            $this->_storeManager->getStore()->getId(),
            $this->_design->getDesignTheme()->getId(),
            $this->httpContext->getValue(CustomerContext::CONTEXT_GROUP),
            'template' => $this->getTemplate(),
            $this->getProducts(),
            $this->getColumnCount()
        ];
    }

    /**
     * Get store identifier
     *
     * @return  int
     */
    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }
}
